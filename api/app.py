########################################################
###
###
###   Template for how to use Celery and Dash inside 
###   a docker-compose type framework
###  
###   The utility of such a framework is that 
###   Celery can process expensive tasks in the background
###   while the application frontend can continue as usual
###
###   This framework also allows you to scale the workers
###   e.g.:   docker-compose up --build --scale worker=4
###
###   
###
###
########################################################
from worker import celery
import celery.states as states

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State, Event

#from components import Entire_Page


#=======================================================================
#===  Creates the dash instance 
#=======================================================================
app    = dash.Dash(__name__)
server = app.server
app.config.suppress_callback_exceptions = True


line_break  =  html.Div( html.Br(), className='row')
output_box  =  dcc.Markdown(id='markdown-box', className='col s6')
input_box   =  dcc.Textarea(id='input-box'   , placeholder='Enter some markdown', className='col s6')
box_row     =  html.Div( [output_box, input_box], className='row')
Entire_Page =  html.Div( [line_break, box_row  ], className='container' )

#=======================================================================
#===
#===  Layout and pulls the layout from components.py
#===
#=======================================================================
app.layout = Entire_Page


#=======================================================================
#===
#===  Callbacks section -- describes the interactivity of the dashboard
#===  e.g. how components (from components.py) interact with one another
#===
#=======================================================================
#---------------------------------------------------------------------------------------
#   Sample of a callback with a celery process
#---------------------------------------------------------------------------------------
@app.callback(Output('markdown-box' , 'children' ) ,
              [Input('input-box'    , 'value'    )])
def update_text(text):
    if text is None:
        return ''' '''
    task = celery.send_task('tasks.add', args=[1, 2], kwargs={})

    #------------------------------------
    # Results of the task executed
    #res = celery.AsyncResult(task.id)
    #if res.state == states.PENDING:
    #    result = res.state
    #else:
    #    result = str(res.result)
    #------------------------------------
    return '''{}  `{}` '''.format(text,task.id)



#========================================================================================
#===
#===  External CSS and JS styles from Materialize CSS -- makes things pretty
#===
#========================================================================================
external_css = ["https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"]
for css in external_css:
    app.css.append_css({"external_url": css})

external_js = ['https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js']
for js in external_js:
    app.scripts.append_script({'external_url': js})



if __name__ == '__main__':
    app.run_server(debug=True)
