# Docker Dash-Celery-Redis

A basic [Docker Compose](https://docs.docker.com/compose/) template for orchestrating a [Dash](https://dash.plot.ly) application & a [Celery](http://www.celeryproject.org/) queue with [Redis](https://redis.io/)

### Background 

This repo was based on insights from several repos that provided templates for composing [celery+flask](https://github.com/mattkohl/docker-flask-celery-redis).  Definitely checkout [https://github.com/mattkohl/docker-flask-celery-redis](https://github.com/mattkohl/docker-flask-celery-redis)

### Installation

Just clone this repo.

### Build & Launch

```bash
docker-compose up -d --build
```

This will expose the Dash application's endpoints on port `5001` as well as a [Flower](https://github.com/mher/flower) server for monitoring workers on port `5555`

To add more workers:
```bash
docker-compose up -d --scale worker=5 --no-recreate
```

To shut down:

```bash
docker-compose down
```

or 

```bash
docker-compose stop; docker-compose rm -f
```
to fully and aggresively shut it all down.

Change exposed port endpoints in [api/app.py](api/app.py)
Define time consuming tasks in [queue/tasks.py](celery-queue/tasks.py) 

---

adapted from [https://github.com/itsrifat/flask-celery-docker-scale](https://github.com/itsrifat/flask-celery-docker-scale)